<%@page import="ees.pojoclases.WORKpojo"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<% WORKpojo ep = (WORKpojo)request.getAttribute("work_details"); %>
<table cellpadding="15">
<tr>
    <td>
        ID:
    </td>
    <td>
        <%=ep.getEid() %>
    </td>
</tr>
<tr>
    <td>PROJECTNAME: </td>
    <td> <%=ep.getProjectname() %></td>
</tr>
<tr>
    <td>TASKNAME: </td>
    <td> <%=ep.getTaskname() %></td>
</tr>
<tr>
    <td>DEADLINE: </td>
    <td> <%=ep.getDeadline()%></td>
</tr>
<tr>
     <td>
         SUBMITTED AT:
     </td>
     <td>
         <%=ep.getSubmitted_at()%>
     </td>
</tr>
<tr>
    <td>
        DELAY TIME:
    </td>
    <td>
        <%=ep.getDelay() %>
    </td>    
</tr>
</table>

</body>
</html>