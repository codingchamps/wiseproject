<%@page import="ees.pojoclases.WORKpojo"%>
<%@page import="ees.pojoclases.STATUSpojo"%>
<%@page import="ees.pojoclases.EMPQUALITIESpojo"%>
<%@page import="ees.pojoclases.EMPpojo"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<table cellpadding="15">
    <th>Details of employee are: </th>
    <% EMPpojo ep = (EMPpojo)request.getAttribute("emppojo");%>
    <tr> 
       <td>
           Id :
       </td>
       <td> 
           <%= ep.getEID() %>
       </td>
    </tr>
    <tr>
    <td>
        Name:
    </td>
    <td>
     <%= ep.getENAME()%>
     </td>
         <tr>
        <td>OB :</td>
        <td><%= ep.getJOB()%></td>
        <tr>
        <td> Manager Number : </td>
        <td><%= ep.getMGRNO()%></td>
        </tr>
        <tr>
        <td>HireDate :</td>
        <td> <%= ep.getHIREDATE()%></td>
        </tr>
        <tr>
        <td>salary :</td>
        <td> <%= ep.getSAL() %></td>
        </tr>
        <tr>
        <td> Department Number:</td>
        <td><%= ep.getDEPTNO() %></td>
        </tr>
        <tr>
        <td>Experience:</td>
        <td><%=request.getAttribute("experience") %></td>
        </tr>
        <%EMPQUALITIESpojo eqp = (EMPQUALITIESpojo)request.getAttribute("empqualpojo");%>
        <tr>
        <td>Communication </td>
        <td> Initiative </td>
        <td>co-operation</td>
        <td>knowledge </td>
        </tr>
        <tr>
        <td><%= eqp.getCommunication() %></td>        
        <td> <%=eqp.getInitiate()%></td>
        <td><%= eqp.getCo_operation() %></td>
        <td> <%= eqp.getKnowledge()%></td>
        </tr>
        <% WORKpojo wp = (WORKpojo)request.getAttribute("workpojo");%>
        <tr>
        <td>Project name: </td>
        <td><%= wp.getProjectname() %></td>
        </tr>
        <tr>
        <td>Task Assigned:</td>
        <td><%= wp.getTaskname() %></td>
        </tr>
        <tr>
        <td>Dead line at:</td>
        <td><%=wp.getDeadline() %></td>
        </tr>
        <tr>
        <td>submitted at: </td>
        <td> <%=wp.getSubmitted_at() %></td>
        </tr>
        <tr>
        <td> delay time is:</td>
        <td><%=wp.getDelay() %> </td>
        </tr>
         <%STATUSpojo sp = (STATUSpojo)request.getAttribute("statuspojo");%>
        <tr>
        <td>Status of the given task is :</td>
        <td><%=sp.getReport()%></td>
        </tr>
        <tr>
        <td></td>
        <td></td>
        </tr>
        </table>
   
   
   

</body>
</html>