<%@page import="java.util.concurrent.TimeUnit"%>
<%@page import="ees.pojoclases.WORKpojo"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<table cellpadding="15 ">
<%WORKpojo WP = (WORKpojo)request.getAttribute("workpojo"); %>
<th> Status Is Succesfully Updated</th>
<tr>
<td>
SUBMITTED_AT:
</td>
  <td><%=WP.getSubmitted_at() %></td>
  </tr>
  <tr>
  <td>
  DELAY:
  </td>
  <td>
  <%String str =  WP.getDelay();
    int uptime = Integer.parseInt(str);
    long hours = TimeUnit.MILLISECONDS.toHours(uptime);
    uptime -= TimeUnit.HOURS.toMillis(hours);

    long minutes = TimeUnit.MILLISECONDS.toMinutes(uptime);
    uptime -= TimeUnit.MINUTES.toMillis(minutes);

    long seconds = TimeUnit.MILLISECONDS.toSeconds(uptime);
    out.println(hours + ":"+minutes + ":" + seconds);
  %>
  </td>
  </tr>
</body>
</html>