package ees.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

import ees.pojoclases.EMPQUALITIESpojo;
import ees.pojoclases.EMPpojo;
import ees.pojoclases.GRADEpojo;
import ees.pojoclases.STATUSpojo;
import ees.pojoclases.WORKpojo;
import ees.servlets.ConnectionFactory;

public class Emp_personalDAO {
	Connection connection = ConnectionFactory.getConnection();
    PreparedStatement ps = null;
    ResultSet rs = null;
    ResultSet rs1 = null;
    String query = "";
    public EMPpojo getEmpDetails(EMPpojo ep) {
    	query = "select * from EMP where EID = " + ep.getEID();
    	
    	try {
			ps = connection.prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			rs.next();
			ep.setEID(rs.getInt(1));
			ep.setENAME(rs.getString(2));
			ep.setJOB(rs.getString(3));
			ep.setMGRNO(rs.getInt(4));
			ep.setHIREDATE((rs.getDate(5)).toString());
			ep.setSAL(rs.getInt(6));
			ep.setDEPTNO(rs.getInt(7));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ep;
    }
    public EMPQUALITIESpojo getEmoQualities(EMPQUALITIESpojo eqp) {
    	query = "select * from EMPQUALITIES where EID = " + eqp.getEid();
    	try{
    		ps = connection.prepareStatement(query);
    		rs = ps.executeQuery();
    		rs.next();
    		eqp.setCommunication(rs.getInt(2));
    		eqp.setInitiate(rs.getInt(3));
    		eqp.setCo_operation(rs.getInt(4));
    		eqp.setKnowledge(rs.getInt(5));
    	}
    	catch(SQLException e){
    		
    	}
		return eqp;
    }	
    public WORKpojo getWorkDetails(WORKpojo wp) {
        query = "select * from WORK where EID = " + wp.getEid();
        
		try {
			ps = connection.prepareStatement(query);
			rs = ps.executeQuery();
			rs.next();
			wp.setProjectname(rs.getString(2));
			wp.setTaskname(rs.getString(3));
			wp.setDeadline((rs.getTimestamp(4)));
			wp.setSubmitted_at(rs.getTimestamp(5));
			wp.setDelay((rs.getString(6)));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return wp;
    }
    public STATUSpojo getStatusDetails(STATUSpojo sp){
    	query = "select * from STATUS where EID = " + sp.getEid();
		try{
    	    ps = connection.prepareStatement(query);
		    rs = ps.executeQuery();
		    rs.next();
		    sp.setReport(rs.getString(2));
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		return sp;
    }
    public GRADEpojo getGradeDetails(GRADEpojo gp) {
    	query = "select * from EMPGRADE where EID = " + gp.getEID();
    	try {
    		ps = connection.prepareStatement(query);
    		rs = ps.executeQuery();
    		rs.next();
    		gp.setQUALITIES(rs.getInt(2));
    		gp.setEXPERIENCE(rs.getInt(3));
    		gp.setDELAY(rs.getInt(4));
    		gp.setGRADE(rs.getString(5));
    	}
    	catch(SQLException e){
    		e.printStackTrace();
    	}
     	return gp;
    }
    public int getEXP(EMPpojo ep) {
    	int exp, join_year, this_year;
    	Calendar now = Calendar.getInstance();
	    join_year = Integer.parseInt(((ep.getHIREDATE()).toString()).substring(0, 4));
	    this_year  = (now.get(Calendar.YEAR));
	    exp = (this_year - join_year);
	    return exp;
    }
}
