package ees.pojoclases;

public class EMPQUALITIESpojo {
	private int eid;
	private int communication;
    private int initiate;
	private int co_operation;
	private int knowledge;
	public int getEid() {
		return eid;
	}
	public void setEid(int eid) {
		this.eid = eid;
	}
	public int getCommunication() {
		return communication;
	}
	public void setCommunication(int communication) {
		this.communication = communication;
	}
	public int getInitiate() {
		return initiate;
	}
	public void setInitiate(int initiate) {
		this.initiate = initiate;
	}
	public int getCo_operation() {
		return co_operation;
	}
	public void setCo_operation(int co_operation) {
		this.co_operation = co_operation;
	}
	public int getKnowledge() {
		return knowledge;
	}
	public void setKnowledge(int knowledge) {
		this.knowledge = knowledge;
	}

}
