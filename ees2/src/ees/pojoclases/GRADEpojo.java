package ees.pojoclases;


public class GRADEpojo {
  private int EID;
  private int QUALITIES;
  private int EXPERIENCE;
  private int DELAY;
  private String GRADE;
public int getEID() {
	return EID;
}
public void setEID(int eID) {
	this.EID = eID;
}
public int getQUALITIES() {
	return QUALITIES;
}
public void setQUALITIES(int qUALITIES) {
	this.QUALITIES = qUALITIES;
}
public int getEXPERIENCE() {
	return EXPERIENCE;
}
public void setEXPERIENCE(int eXPERIENCE) {
	this.EXPERIENCE = eXPERIENCE;
}
public int getDELAY() {
	return DELAY;
}
public void setDELAY(int dELAY) {
	this.DELAY = dELAY;
}
public String getGRADE() {
	return GRADE;
}
public void setGRADE(String GRADE) {
	this.GRADE = GRADE;
}
	
}
