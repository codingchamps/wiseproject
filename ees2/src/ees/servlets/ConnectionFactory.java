package ees.servlets;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
	static Connection connection = null;
    static public Connection getConnection() {
        try {
		    Class.forName("com.mysql.jdbc.Driver");
		    connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/Employee_Evaluation_System_DB", "root", "root");
        }
        catch(ClassNotFoundException e) {
    	    e.printStackTrace();
        }
        catch(SQLException e){
        	e.printStackTrace();
        }
        return connection;
    }
}
