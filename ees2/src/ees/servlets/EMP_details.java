package ees.servlets;

import java.io.IOException;

//import java.io.PrintWriter;
import java.util.Calendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ees.dao.Emp_personalDAO;
import ees.pojoclases.EMPQUALITIESpojo;
import ees.pojoclases.EMPpojo;
import ees.pojoclases.GRADEpojo;
import ees.pojoclases.LOG_INpojo;
import ees.pojoclases.STATUSpojo;
import ees.pojoclases.WORKpojo;

/**
 * Servlet implementation class EMP_details
 */
public class EMP_details extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EMP_details() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doprocess(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doprocess(request, response);
	}
	public void doprocess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
		int emp_id = Integer.parseInt(((session.getAttribute("EID")).toString()));
		
		Emp_personalDAO epd = new Emp_personalDAO();
		
		EMPpojo ep = new EMPpojo();
		ep.setEID(emp_id);
		ep = epd.getEmpDetails(ep);
		request.setAttribute("emppojo", ep);
			
		EMPQUALITIESpojo eqp = new EMPQUALITIESpojo();
		eqp.setEid(emp_id);
		eqp = epd.getEmoQualities(eqp);	
		request.setAttribute("empqualpojo", eqp);
				
		LOG_INpojo lp = new LOG_INpojo();
		lp.setEid(emp_id);
		
		
		STATUSpojo sp = new STATUSpojo();
		sp.setEid(emp_id);
		sp = epd.getStatusDetails(sp);
		request.setAttribute("statuspojo", sp);
		
		WORKpojo wp = new WORKpojo();
		wp.setEid(emp_id);
		wp = epd.getWorkDetails(wp);	
		request.setAttribute("workpojo", wp);	

		GRADEpojo gp = new GRADEpojo();
		gp.setEID(emp_id);
		gp = epd.getGradeDetails(gp);
		request.setAttribute("gradepojo", gp);
		
		request.setAttribute("experience", epd.getEXP(ep));
		
	    RequestDispatcher rd = request.getRequestDispatcher("EMP_display.jsp");
		rd.include(request, response);
	}
}


