package ees.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ees.dao.Emp_statusUpDAO;
import ees.pojoclases.STATUSpojo;
import ees.pojoclases.WORKpojo;

/**
 * Servlet implementation class EMP_statusupdate
 */
public class EMP_statusupdate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EMP_statusupdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(request,response);

}

	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
 	    int count = 0;
		response.setContentType("text/html");
		PrintWriter pw =response.getWriter();		
		int id;
		HttpSession session = request.getSession();
		id = Integer.parseInt((session.getAttribute("EID").toString()));
		WORKpojo wp =new WORKpojo();
		wp.setEid(id);				
		String state = request.getParameter("status");
		if(state.equalsIgnoreCase("completed")) {
		    STATUSpojo sp=new STATUSpojo();
		    sp.setReport("state");
		    sp.setEid(id);
		
		    Emp_statusUpDAO esD=new Emp_statusUpDAO();
		
		    wp = esD.setInfo(wp);
		    wp = esD.getDetails(wp);
		    count = esD.into_Db(wp);
		}
		else
			pw.println("status pending");
		if(count != 0){
		    request.setAttribute("workpojo", wp);
		    RequestDispatcher rd= request.getRequestDispatcher("EMP_StatusDisplay.jsp");
		    rd.include(request,response);
		}
		else
			pw.println("status not updated");
	}


}
