package ees.servlets;

import java.io.IOException;

import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ees.dao.EMP_workCHECKDAO;
import ees.pojoclases.WORKpojo;

/**
 * Servlet implementation class EMP_workCHECK
 */
public class EMP_workCHECK extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EMP_workCHECK() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doprocess(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doprocess(request, response);
	}
	protected void doprocess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw = response.getWriter();
		HttpSession session = request.getSession();
		int EID = Integer.parseInt(session.getAttribute("EID").toString()); 
		System.out.println("EID" + EID);
		WORKpojo wp = new WORKpojo();
		wp.setEid(EID);
		EMP_workCHECKDAO ewd = new EMP_workCHECKDAO();
		response.setContentType("text/html");
		try {
			if(ewd.workAllocated(EID)){
			    wp = ewd.getDetails(wp);
			    request.setAttribute("work_details", wp);
				RequestDispatcher rd = request.getRequestDispatcher("EMP_workCHECK.jsp");
				rd.include(request, response);
			}
			else
				pw.println("work not yet allocated");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}	
}


