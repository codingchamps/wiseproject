package ees.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ees.dao.*;
import ees.pojoclases.*;

/**
 * Servlet implementation class Gradeservlet
 */
public class Gradeservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Gradeservlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doProcess(request,response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(request,response);
	}
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		int emp_id = Integer.parseInt(((session.getAttribute("EID")).toString()));
		
		WORKpojo wp = new WORKpojo();
		wp.setEid(emp_id);
		
		GRADEpojo gp = new GRADEpojo();
		gp.setEID(emp_id);
		
		EMPpojo ep = new EMPpojo();
		ep.setEID(emp_id);
		
		EMPQUALITIESpojo eqp = new EMPQUALITIESpojo();
		eqp.setEid(emp_id);
		
		GradeDAO  gd  = new GradeDAO();
		
		if (gd.setdetails(gp, ep, eqp, wp)) {
			System.out.println("Inserted");
			request.setAttribute("Gradepojo", gp);
			RequestDispatcher rd = request.getRequestDispatcher("FINALGRADE.jsp");
			rd.include(request, response);
		}
		else
			System.out.println("Not inserted");
		
		
		}
}
