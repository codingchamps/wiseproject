package ees.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ees.dao.LoginDAO;
import ees.pojoclases.EMPpojo;

/**
 * Servlet implementation class LoginCheck
 */
public class LoginCheck extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginCheck() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doprocess(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doprocess(request, response);
	}
	protected void doprocess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = null;
		PrintWriter pw = response.getWriter();
		int EID=Integer.parseInt(request.getParameter("eid"));
        String pass = request.getParameter("psw");
        EMPpojo ep = new EMPpojo();
        ep.setEID(EID);
        LoginDAO ld = new LoginDAO();
        response.setContentType("text/html");
        HttpSession session = request.getSession();
        session.setAttribute("EID", EID);
        try {
			if(ld.isuser(EID, pass)){
				if(ld.isManager(EID)) {
					rd = request.getRequestDispatcher("MGR_login.jsp");
					rd.include(request, response);
				}
				else {
					rd = request.getRequestDispatcher("EMP_login.jsp");
					rd.include(request, response);
				}
				
			}
			else
				pw.println("check user ID and Password");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}


}
