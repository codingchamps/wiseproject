package ees.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ees.dao.MGR_assignWORKDAO;
import ees.pojoclases.WORKpojo;

/**
 * Servlet implementation class MGR_assignWORK
 */
public class MGR_assignWORK extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MGR_assignWORK() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doprocess(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doprocess(request, response);
	}
	protected void doprocess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw = response.getWriter();
		int empid = Integer.parseInt(request.getParameter("empid"));
		String proname =request.getParameter("pro_name");
		String taskname = request.getParameter("task_name");
		Date deadline = null;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
		try{
		deadline = simpleDateFormat.parse((request.getParameter("deadline")));
		}
		catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WORKpojo wp = new WORKpojo();
		
		wp.setEid(empid);
		wp.setProjectname(proname);
		wp.setTaskname(taskname);
		wp.setDeadline(deadline);
		
		MGR_assignWORKDAO mdD = new MGR_assignWORKDAO();
		
		try{
		    if ((mdD).isuser(wp.getEid())){
			     mdD.insert(wp); 
				 request.setAttribute("work", wp);
			 	 RequestDispatcher rd = request.getRequestDispatcher("MGR_DisplayAssignWORK.jsp");
				 rd.include(request, response);
		}
		else {
			pw.print("unknown user");
		}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		 
	}

	

	
		
}
