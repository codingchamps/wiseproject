package ees.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ees.dao.MGR_displayDAO;
import ees.pojoclases.EMPQUALITIESpojo;
import ees.pojoclases.EMPpojo;
import ees.pojoclases.LOG_INpojo;
import ees.pojoclases.STATUSpojo;
import ees.pojoclases.WORKpojo;

/**
 * Servlet implementation class MGR_details
 */
public class MGR_details extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MGR_details() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doprocess(request, response);
		

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doprocess(request, response);
	}
	public void doprocess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException  {
		PrintWriter pw = response.getWriter();
        int emp_id = Integer.parseInt((request.getParameter("emp_id")));
		
		MGR_displayDAO mdD = new MGR_displayDAO();
		
		
		if(mdD.isemp(emp_id) && mdD.isNOTManager(emp_id)) {
			
		    EMPpojo ep = new EMPpojo();
			ep.setEID(emp_id);
			ep = mdD.getEmpDetails(ep);
		    
		    
			
			EMPQUALITIESpojo eqp = new EMPQUALITIESpojo();
			eqp.setEid(emp_id);
			
			LOG_INpojo lp = new LOG_INpojo();
			lp.setEid(emp_id);
			
			STATUSpojo sp = new STATUSpojo();
			sp.setEid(emp_id);
			
			WORKpojo wp = new WORKpojo();
			wp.setEid(emp_id);
			
		    
		    RequestDispatcher rd = request.getRequestDispatcher("MGR_display.jsp");
		    request.setAttribute("experience", mdD.getEXP(ep));
		    eqp = mdD.getEmoQualities(eqp);
		    
		    sp = mdD.getStatusDetails(sp);
		    
		    wp = mdD.getWorkDetails(wp);
		    
		    request.setAttribute("emppojo", ep);
		    request.setAttribute("empqualpojo", eqp);
		    request.setAttribute("statuspojo", sp);
		    request.setAttribute("workpojo", wp);	
		    
		    rd.include(request, response);
		}
		else
			pw.println("not found");
	}

}
